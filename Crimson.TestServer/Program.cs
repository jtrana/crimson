﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Communication;
using Crimson.Controls;

namespace Crimson.TestServer
{
    public delegate string OneWay();
    public delegate string TwoWay();

    class Program
    {
        static void Main(string[] args)
        {
            var server = new CommandServer();
            server.Initialize(
                ctx =>
                {
                    var module = new DummyModule();
                    module.Initialize(ctx);
                }
                );

            Console.ReadLine();

            

            //PathConverter.Test();
        }

        public void DoStuff()
        {
            AcceptsConvertible((Func<string>)(() => "Hello world"));
        }

        public void AcceptsConvertible(Convertible c)
        {

        }

        public class Convertible
        {
            public static implicit operator Convertible(Func<string> func)
            {
                return null;
            }
        }
    }
}
