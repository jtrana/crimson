﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Crimson.Core.Bindings
{
    public delegate BindableBindingWrapper<T> Binder<T>();

    public class Binding<T>
    {
        private List<Bindable> m_DependentBindables;

        private Binder<T> m_VMBindableGetter;
        private Bindable<T> m_VMBindable;
        private UIBindable<T> m_UIBindable;
        private BindingMode m_Mode;

        public Binding(Binder<T> vmBindableGetter)
        {
            m_DependentBindables = new List<Bindable>();
            m_VMBindableGetter = vmBindableGetter;
        }

        public static implicit operator Binding<T>(Binder<T> getter)
        {
            return new Binding<T>(getter);
        }

        public static implicit operator Binding<T>(Func<Bindable<T>> getter)
        {
            return new Binding<T>(() => new BindableBindingWrapper<T>(getter(), BindingMode.ToUI));
        }

        public void SupplyUISide(UIBindable<T> uiBindable)
        {
            m_UIBindable = uiBindable;
            RefreshBinding();
        }

        private void UnhookBinding()
        {
            switch (m_Mode)
            {
                case BindingMode.ToUI:
                    if(m_VMBindable!=null)
                        m_VMBindable.OnValueChanged -= NotifyUI;
                    break;
                case BindingMode.ToVM:
                    if(m_UIBindable!=null)
                        m_UIBindable.OnValueChanged -= NotifyVM;
                    break;
                case BindingMode.TwoWay:
                    if(m_VMBindable!=null)
                        m_VMBindable.OnValueChanged -= NotifyUI;
                    if(m_UIBindable!=null)
                        m_UIBindable.OnValueChanged -= NotifyVM;
                    break;
            }
        }

        private void HookBinding()
        {
            switch (m_Mode)
            {
                case BindingMode.ToUI:
                    if (m_VMBindable != null)
                        m_VMBindable.OnValueChanged += NotifyUI;
                    break;
                case BindingMode.ToVM:
                    if (m_UIBindable != null)
                        m_UIBindable.OnValueChanged += NotifyVM;
                    break;
                case BindingMode.TwoWay:
                    if (m_VMBindable != null)
                        m_VMBindable.OnValueChanged += NotifyUI;
                    if (m_UIBindable != null)
                        m_UIBindable.OnValueChanged += NotifyVM;
                    break;
            }
        }

        private void NotifyUI(T oldValue, T newValue)
        {
            //Debug.WriteLine("Refreshing binding on " + m_UIBindable.Element.GetType() + " for UI");
            m_UIBindable.Value = newValue;
        }

        private void NotifyVM(T oldValue, T newValue)
        {
            //Debug.WriteLine("Refreshing binding on " + m_UIBindable.Element.GetType() + " for VM");
            m_VMBindable.Value = newValue;
        }

        private void RefreshBinding()
        {
            UnhookBinding();
            foreach (var bindable in m_DependentBindables)
                bindable.OnValueChangedObject -= OnValueChangedWrapper;
            //Debug.WriteLine("Session open");
            Bindable.OpenSession();
            var wrapper = m_VMBindableGetter();
            m_Mode = wrapper.Mode;
            m_VMBindable = wrapper.Bindable;
            Bindable.CloseSession();
            //Debug.WriteLine("Session closed");
            var dependentBindables = new List<Bindable>(Bindable.Session.Value);
            Bindable.Session.Value.Clear();
            if (dependentBindables.Count > 0 && dependentBindables[dependentBindables.Count - 1] == m_VMBindable)
                dependentBindables.RemoveAt(dependentBindables.Count - 1);
            m_DependentBindables = dependentBindables;
            foreach (var bindable in m_DependentBindables)
            {
                Debug.WriteLine("Session - " + bindable.GetHashCode()+":"+m_UIBindable.Element.GetType()+" affected by "+bindable.GetType());
                bindable.OnValueChangedObject += OnValueChangedWrapper;
            }
            HookBinding();
            m_UIBindable.Value = m_VMBindable.Value;
            //clear existing bindable hooks
            //open session
            //get bindable
            //close session
            //snag list of dependent bindables
            //remove last if = vmBindable
            //wire up handlers on dependent ones to refresh the binding
        }

        private void OnValueChangedWrapper(object oldObject, object newObject)
        {
            if (oldObject != null)
            {
                Debug.WriteLine("Change on " + oldObject.GetType() + " affecting element of type " + this.GetType());
            }
            RefreshBinding();
        }
    }
}
