﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crimson.Core.Bindings
{
    public class UIBindable<T> : Bindable<T>
    {
        private Element m_Element;
        public Element Element { get { return m_Element; } }

        public UIBindable(Element parent, T initialValue) : base(initialValue)
        {
            m_Element = parent;
            OnValueChanged += HandleValueChange;
        }

        public void MarkRenderDirty()
        {
            m_Element.Context.MarkElementAsRenderDirty(m_Element);
        }

        private void HandleValueChange(T oldValue, T newValue)
        {
            MarkRenderDirty();
        }
    }
}
