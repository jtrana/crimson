﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crimson.Core.Bindings
{
    public enum BindingMode
    {
        ToUI,
        TwoWay,
        ToVM
    }

    public class BindableBindingWrapper<T>
    {
        public Bindable<T> Bindable { get; set; }
        public BindingMode Mode { get; set; }

        public BindableBindingWrapper(Bindable<T> bindable, BindingMode mode)
        {
            Bindable = bindable;
            Mode = mode;
        }
    }
}
