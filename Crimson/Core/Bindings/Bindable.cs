﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Diagnostics;
using System.Threading;

namespace Crimson.Core.Bindings
{
    public delegate void OnValueChangedObjectHandler(object oldValue, object newValue);

    public class Bindable
    {
        public static ThreadLocal<List<Bindable>> Session
            = new ThreadLocal<List<Bindable>>(()=>new List<Bindable>());
        public static ThreadLocal<bool> IsSessionOpen
            = new ThreadLocal<bool>(() => false);

        public static void OpenSession()
        {
            Session.Value.Clear();
            IsSessionOpen.Value = true;
        }

        public static void CloseSession()
        {
            IsSessionOpen.Value = false;
        }

        protected void FireOnValueChangedObject(object oldValue, object newValue)
        {
            if (OnValueChangedObject != null)
                OnValueChangedObject(oldValue, newValue);
        }

        public event OnValueChangedObjectHandler OnValueChangedObject;
    }


    public delegate void OnValueChangedHandler<T>(T oldValue, T newValue);

    public class Bindable<T> : Bindable
    {
        public Bindable(T initialValue) : this(initialValue, true)
        {
        }

        public Bindable(T initialValue, bool autoFreeze)
        {
            m_Value = initialValue;
            AutoFreeze = autoFreeze && typeof(T).IsSubclassOf(typeof(Freezable));
            if (AutoFreeze)
                (initialValue as Freezable).Freeze();
        }

        public bool AutoFreeze { get; private set; }

        protected T m_Value;
        public virtual T Value
        {
            get
            {
                if (Bindable.IsSessionOpen.Value)
                {
                    //Debug.WriteLine("Value " + GetHashCode());
                    Bindable.Session.Value.Add(this);
                }
                return m_Value;
            }
            set
            {
                if (EqualityComparer<T>.Default.Equals(m_Value, value))
                    return;
                var oldValue = m_Value;
                m_Value = value;
                if (AutoFreeze)
                    (m_Value as Freezable).Freeze();
                if (OnValueChanged != null)
                    OnValueChanged(oldValue, m_Value);
                FireOnValueChangedObject(oldValue, m_Value);
            }
        }

        public event OnValueChangedHandler<T> OnValueChanged;

        public static implicit operator Bindable<T>(T defaultValue)
        {
            return new Bindable<T>(defaultValue);
        }

        public BindableBindingWrapper<T> ToUI
        {
            get { return new BindableBindingWrapper<T>(this, BindingMode.ToUI); }
        }

        public BindableBindingWrapper<T> TwoWay
        {
            get { return new BindableBindingWrapper<T>(this, BindingMode.TwoWay); }
        }

        public BindableBindingWrapper<T> ToVM
        {
            get { return new BindableBindingWrapper<T>(this, BindingMode.ToVM); }
        }
    }
}
