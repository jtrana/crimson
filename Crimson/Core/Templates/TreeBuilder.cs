﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Crimson.Core.Bindings;

namespace Crimson.Core.Templates
{
    public class TreeBuilder
    {
        public TreeBuilder Parent { get; set; }
        public List<TreeBuilder> Children { get; set; }
        public Func<Context, Element> ControlTemplate { get; set; }

        public TreeBuilder(Func<Context,Element> controlTemplate)
        {
            Parent = null;
            Children = new List<TreeBuilder>();
            ControlTemplate = controlTemplate;
        }

        private TreeBuilder() : this(ctx=>new Element(ctx))
        {
        }

        public static TreeBuilder operator -(TreeBuilder a)
        {
            if (a.Parent == null)
            {
                var parent = new TreeBuilder();
                a.Parent = parent;
            }
            a.Parent.Children.Add(a);
            return a;
        }

        public static TreeBuilder operator -(TreeBuilder a, TreeBuilder b)
        {
            if (a.Parent == null)
            {
                var parent = new TreeBuilder();
                a.Parent = parent;
            }
            a.Parent.Children.Add(b);
            b.Parent = a.Parent;
            return b;
        }

        public TreeBuilder this[TreeBuilder childRoot]
        {
            get
            {
                this.Children.AddRange(childRoot.Parent.Children);
                foreach (var child in childRoot.Children)
                    child.Parent = this;
                return this;
            }
        }

        public Element Instantiate(Context ctx)
        {
            var root = ControlTemplate(ctx);
            foreach (var child in Children)
                root.AddChild(child.Instantiate(ctx));
            return root;
        }

        public static TreeBuilder Generate<T>(Func<Context, T> instantiator, Action<T> style, params Action<T>[] bindings) where T : Element
        {
            return new TreeBuilder(ctx =>
            {
                var instance = instantiator(ctx);
                DefaultStyleRegistry.Apply(instance);
                if (style != null)
                    style(instance);
                foreach (var binding in bindings)
                    binding(instance);
                return instance;
            });
        }
    }
}
