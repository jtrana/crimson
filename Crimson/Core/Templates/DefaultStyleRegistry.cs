﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crimson.Core.Templates
{
    public static class DefaultStyleRegistry
    {
        private static Dictionary<Type, object> m_StyleRegistry;

        static DefaultStyleRegistry()
        {
            m_StyleRegistry = new Dictionary<Type, object>();
        }

        public static void Register<T>(Action<T> style)
        {
            m_StyleRegistry.Add(typeof(T), style);
        }

        public static void Apply<T>(T instance)
        {
            object result;
            if(m_StyleRegistry.TryGetValue(typeof(T), out result))
            {
                if(result!=null)
                    ((Action<T>)result)(instance);
            }
        }
    }
}
