﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crimson.Core.Templates
{
    public class ViewMappingRegistry
    {
        private Context m_Context;
        private Dictionary<Type, object> m_Registry;

        public ViewMappingRegistry(Context ctx)
        {
            m_Context = ctx;
            m_Registry = new Dictionary<Type, object>();
        }

        public void RegisterView<T>(Func<T, TreeBuilder> vmToView)
        {
            m_Registry.Add(typeof(T), vmToView);
        }

        public Element GenerateNonGeneric(object o)
        {
            var untypedMethod = typeof(ViewMappingRegistry).GetMethod("Generate");
            var typedMethod = untypedMethod.MakeGenericMethod(o.GetType());
            return typedMethod.Invoke(this, new[]{ o }) as Element;
        }

        public Element Generate<T>(T instance)
        {
            object result;
            if (!m_Registry.TryGetValue(typeof(T), out result))
                throw new InvalidOperationException("View Mapping does not exist for " + typeof(T));
            return ((Func<T, TreeBuilder>)result)(instance).Instantiate(m_Context);
        }
    }
}
