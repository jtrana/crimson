﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Crimson.Core.Shapes;
using Crimson.Core.Styles;
using Crimson.Communication;
using Crimson.Core.Events.Input;
using System.Windows;
using Crimson.Core.Events.Graphics;
using System.Threading;
using Crimson.Core.Templates;
using System.Windows.Input;
using System.Windows.Forms;
using Crimson.Core.Utils;
using Clipboard = Crimson.Core.Utils.Clipboard;
using System.Diagnostics;

namespace Crimson.Core
{
    public delegate void KeyUpHandler(Keys key);
    public delegate void KeyDownHandler(Keys key);

    public class Context
    {
        #region Internal Classes

        public class Session : IDisposable
        {
            private Action m_CloseSession;

            public Session(Action open, Action close)
            {
                m_CloseSession = close;
                open();
            }

            public void Dispose()
            {
                m_CloseSession();
            }
        }

        private class EmbeddedShape
        {
            public Shape Shape { get; set; }
            public Matrix InvertedTransform { get; set; }

            public EmbeddedShape(Shape shape, Matrix matrix)
            {
                Shape = shape;
                InvertedTransform = matrix;
            }
        }

        #endregion

        #region Initialization

        private ApplicationInstance m_Application;
        private Dictionary<Element, List<EmbeddedShape>> m_ShapeRegistry;
        private List<Element> m_PointerLeaveTrackingList;

        private List<EmbeddedShape> m_CurrentShapeList;

        private Stack<Matrix> m_TransformStack;

        private Mutex m_RenderSessionMutex;
        private Dictionary<int,Element> m_DirtyRenderList;

        public ViewMappingRegistry ViewMapping { get; private set; }

        private VirtualKeyboard m_Keyboard;

        public Context()
        {
            m_Keyboard = new VirtualKeyboard();
            Clipboard = new Clipboard();
            ViewMapping = new ViewMappingRegistry(this);
            m_ShapeRegistry = new Dictionary<Element, List<EmbeddedShape>>();
            m_TransformStack = new Stack<Matrix>();
            m_DirtyRenderList = new Dictionary<int, Element>();
            m_RenderSessionMutex = new Mutex();
            m_PointerLeaveTrackingList = new List<Element>();
            m_TransformStack.Push(Matrix.Identity);
        }

        public void Initialize(ApplicationInstance device)
        {
            m_Application = device;
        }

        #endregion Initialization

        #region Inbound Messages

        public void ReceiveEventKeyUp(int code)
        {
            using (OpenRenderSession())
            {
                if (OnKeyUp != null)
                    OnKeyUp((Keys)code);
                m_Keyboard.SendKey((Keys)code, false); 
            }
        }

        public event KeyUpHandler OnKeyUp;

        public void ReceiveEventKeyDown(int code)
        {
            using (OpenRenderSession())
            {
                if (OnKeyDown != null)
                    OnKeyDown((Keys)code);
                m_Keyboard.SendKey((Keys)code, true);
            }
        }

        public event KeyDownHandler OnKeyDown;

        public VirtualKeyboard Keyboard { get { return m_Keyboard; } }

        public Clipboard Clipboard { get; private set; }

        public void ReceiveEvent(PointerUpEvent Event)
        {
            using(OpenRenderSession())
                PerformHitTest<PointerUpEvent>(Event.Location, v => v.OnPointUp, Event, false);
        }

        public void ReceiveEvent(PointerDownEvent Event)
        {
            using (OpenRenderSession())
                PerformHitTest<PointerDownEvent>(Event.Location, v => v.OnPointDown, Event, false);
        }

        public void ReceiveEvent(PointerMoveEvent Event)
        {
            using (OpenRenderSession())
                PerformHitTest<PointerMoveEvent>(Event.Location, v => v.OnPointMove, Event, true);
        }


        public void PerformHitTest<T>(Point toTest, Func<Element, Func<T, Shape, Point, bool>> evt, T evtData, bool leaveTracking)
        {
            var newLeaveList = new List<Element>();
            foreach (var kvp in m_ShapeRegistry)
            {
                if (!kvp.Key.IsPickable)
                    continue;
                bool wasHit = false;
                foreach (var embedded in kvp.Value.Reverse<EmbeddedShape>())
                {
                    var transformedPoint = embedded.InvertedTransform.Transform(toTest);
                    if (embedded.Shape.Bounds.FillContains(transformedPoint))
                    {
                        var evtInstance = evt(kvp.Key);
                        if (evtInstance(evtData, embedded.Shape, transformedPoint))
                        {
                            if (leaveTracking && kvp.Key.IsPointerLeaveTrackable)
                                newLeaveList.Add(kvp.Key);
                            wasHit = true;
                            break;
                        }
                    }
                }
                if (wasHit)
                    break;
            }
            if (!leaveTracking)
                return;
            var oldList = m_PointerLeaveTrackingList;
            m_PointerLeaveTrackingList = newLeaveList;
            foreach (var oldElement in oldList)
                if (!newLeaveList.Contains(oldElement))
                    oldElement.OnPointerLeave();
        }

        #endregion Inbound Messages

        #region Element Scope

        public void Cleanup(Element element)
        {
            Debug.WriteLine("Disposing " + element.GetHashCode());
            if(m_DirtyRenderList.ContainsKey(element.GetHashCode()))
                m_DirtyRenderList.Remove(element.GetHashCode());
            List<EmbeddedShape> currentShapes;
            if (m_ShapeRegistry.TryGetValue(element, out currentShapes))
            {
                foreach (var oldShape in currentShapes)
                {
                    var shapeRemovedEvent = new ShapeRemovedEvent(oldShape.Shape);
                    m_Application.Send(shapeRemovedEvent);
                }
                currentShapes.Clear();
                m_ShapeRegistry.Remove(element);
            }
        }

        private void OpenElementScope(Element element)
        {
            List<EmbeddedShape> currentShapes;
            if (m_ShapeRegistry.TryGetValue(element, out currentShapes))
            {
                foreach (var oldShape in currentShapes)
                {
                    var shapeRemovedEvent = new ShapeRemovedEvent(oldShape.Shape);
                    m_Application.Send(shapeRemovedEvent);
                }
                currentShapes.Clear();
            }
            else
            {
                currentShapes = new List<EmbeddedShape>();
                m_ShapeRegistry.Add(element, currentShapes);
            }
            if (element.IsPickable)
                m_CurrentShapeList = currentShapes;
            else
                m_CurrentShapeList = null;
            Push(element.CumulativeTransform);
        }

        private void CloseElementScope()
        {
            Pop();
            m_CurrentShapeList = null;
        }

        public IDisposable OpenElementSession(Element element)
        {
            return new Session(()=>OpenElementScope(element), ()=>CloseElementScope());
        }

        #endregion Element Scope

        #region Render Session

        public IDisposable OpenRenderSession()
        {
            m_RenderSessionMutex.WaitOne();
            return new Session(() => OpenRenderScope(), () => CloseRenderScope());
        }

        private void OpenRenderScope()
        {
        }

        private void CloseRenderScope()
        {
            while (m_DirtyRenderList.Count > 0)
            {
                var toRenderKVP = m_DirtyRenderList.First();
                toRenderKVP.Value.Render();
                m_DirtyRenderList.Remove(toRenderKVP.Key);
            }
            m_DirtyRenderList.Clear();
            m_Application.Send(new RenderTimeEvent());
            m_RenderSessionMutex.ReleaseMutex();
        }

        public void ForceRender()
        {
            CloseRenderScope();
            OpenRenderScope();
        }

        public void MarkElementAsRenderDirty(Element element)
        {
            //only add
            var toRemoveList = new List<int>();
            foreach (var dirtyKVP in m_DirtyRenderList)
            {
                //does the element in question already have a parent
                //in the list?
                var current = element;
                while (current != null)
                {
                    if (current == dirtyKVP.Value)
                        return;
                    current = current.Parent.Value;
                }
                //if it didn't already have a parent in the list
                //is this a child of the element in question?
                //if so, this new entry would supersede it
                current = dirtyKVP.Value;
                while (current != null)
                {
                    if (current == element)
                    {
                        toRemoveList.Add(dirtyKVP.Value.GetHashCode());
                        break;
                    }
                    current = current.Parent.Value;
                }
            }
            //add the element
            m_DirtyRenderList.Add(element.GetHashCode(), element);
            //remove any elements that were children of the new addition
            foreach (var key in toRemoveList)
                m_DirtyRenderList.Remove(key);
        }

        #endregion

        #region Ops

        public void Push(Transform transform)
        {
            var current = m_TransformStack.Peek();
            var newMatrix = current * transform.Value;
            m_TransformStack.Push(newMatrix);
        }

        public void Pop()
        {
            m_TransformStack.Pop();
        }

        private Matrix AddCore(Shape shape)
        {
            var transform = m_TransformStack.Peek();
            var invertTransform = transform;
            invertTransform.Invert();
            var embedded = new EmbeddedShape(shape, invertTransform);
            if(m_CurrentShapeList!=null)
                m_CurrentShapeList.Add(embedded);
            return transform;
        }

        public void AddInvisible(Shape shape)
        {
            AddCore(shape);
        }


        public void Add(Rectangle rectangle)
        {
            var transform = AddCore(rectangle);
            m_Application.Send(new RectangleAddedEvent(rectangle, transform));
        }

        public void Add(Path path)
        {
            var transform = AddCore(path);
            m_Application.Send(new PathAddedEvent(path, transform));
        }

        public void Add(Text text)
        {
            var transform = AddCore(text);
            m_Application.Send(new TextAddedEvent(text, transform));

            /*
            var invertTransform = transform;
            invertTransform.Invert();
            var rectangle = new Rectangle(((RectangleGeometry)text.Bounds).Rect);
            rectangle.Fill = new SolidFillStyle(Color.FromArgb(50, 0, 0, 255));
            var embeddedRect = new EmbeddedShape(rectangle, invertTransform);
            m_CurrentShapeList.Add(embeddedRect);
            m_Application.Send(new RectangleAddedEvent(rectangle, transform));
             * */
        }

        #endregion Ops
    }
}
