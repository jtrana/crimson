﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Templates;
using Crimson.Controls;
using Crimson.Core.Bindings;
using System.Windows.Media;
using Crimson.Core.Elements;
using Crimson.Core.Shapes;

namespace Crimson.Core
{
    public static class Stock
    {
        public static TreeBuilder Square(Action<Square> Style = null, Binder<Color> Color = null)
        {
            return TreeBuilder.Generate<Square>(
                ctx=>new Square(ctx),
                Style,
                r=> BindIfNotNull(Color,r.Fill)
                );
        }

        public static TreeBuilder TestElement(Action<TestElement> Style = null, Binder<Color> Fill = null, Binder<string> Text = null)
        {
            return TreeBuilder.Generate<TestElement>(
                ctx => new TestElement(ctx),
                Style,
                r => BindIfNotNull(Fill, r.FillColor),
                r => BindIfNotNull(Text, r.Text)
                );
        }

        public static TreeBuilder Placeholder(Action<Placeholder> Style = null, Binder<object> Source = null)
        {
            return TreeBuilder.Generate<Placeholder>(
                ctx => new Placeholder(ctx),
                Style,
                p => BindIfNotNull(Source, p.Source)
                );
        }

        public static TreeBuilder TextLine(Action<TextLine> Style = null, Binder<Text> Text = null, Binder<string> Message = null)
        {
            return TreeBuilder.Generate<TextLine>(
                ctx => new TextLine(ctx),
                Style,
                el => BindIfNotNull(Text, el.Text),
                el => BindIfNotNull(Message, el.Message)
                );
        }

        public static TreeBuilder SelectableTextLine(Action<SelectableTextLine> Style = null, Binder<Text> Text = null, Binder<string> Message = null, Binder<string> SelectedText = null)
        {
            return TreeBuilder.Generate<SelectableTextLine>(
                ctx => new SelectableTextLine(ctx),
                Style,
                el => BindIfNotNull(Text, el.Text),
                el => BindIfNotNull(Message, el.Message),
                el => BindIfNotNull(SelectedText, el.SelectedText)
                );
        }

        public static TreeBuilder Stacker<T>(Action<Stacker<T>> Style = null, Binder<IList<T>> Source = null, Binder<StackerOrientation> Orientation = null)
        {
            return TreeBuilder.Generate<Stacker<T>>(
                ctx => new Stacker<T>(ctx),
                Style,
                el => BindIfNotNull(Source, el.Source),
                el => BindIfNotNull(Orientation, el.Orientation)
               );
        }

        public static TreeBuilder Button(Action<Button> Style = null, Binder<Command> Command = null)
        {
            return TreeBuilder.Generate<Button>(
                ctx => new Button(ctx),
                Style,
                el => BindIfNotNull(Command, el.Command)
                );
        }

        private static void BindIfNotNull<T>(Binder<T> binder, UIBindable<T> uiBindable)
        {
            if (binder != null)
            {
                var binding = new Binding<T>(binder);
                binding.SupplyUISide(uiBindable);
            }
        }

        //TODO
        //Set all common Element properties here
        public static TreeBuilder With(this TreeBuilder builder, Binder<Transform> Transform = null)
        {
            Func<Context, Element> localControlTemplateCopy = builder.ControlTemplate;
            Func<Context, Element> doMoreToElement = ctx =>
            {
                var originalElement = localControlTemplateCopy(ctx);
                BindIfNotNull(Transform, originalElement.Transform);
                return originalElement;
            };
            builder.ControlTemplate = doMoreToElement;
            return builder;
        }
    }

}
