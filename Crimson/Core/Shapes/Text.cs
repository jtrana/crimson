﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Globalization;
using System.Windows;
using Crimson.Core.Bindings;

namespace Crimson.Core.Shapes
{
    [Flags]
    public enum FontModifier
    {
        Normal,
        Italic,
        Bold
    }

    public class Text : Shape
    {
        public readonly Bindable<FontModifier> Modifiers;
        public readonly Bindable<string> Face;
        public readonly Bindable<int> FontHeight; //em
        public readonly Bindable<int> LineHeight;
        public readonly Bindable<string> Message;

        public FormattedText BackingText { get; private set; }
        public GlyphTypeface GlyphTypeface { get; private set; }

        public Text() : this(string.Empty)
        {
        }

        public Text(string initialMessage)
        {
            Modifiers = FontModifier.Normal;
            Face = "arial";
            FontHeight = 50;
            LineHeight = 60;
            Message = initialMessage;

            Modifiers.OnValueChangedObject += OnValueChanged;
            Face.OnValueChangedObject += OnValueChanged;
            FontHeight.OnValueChangedObject += OnValueChanged;
            LineHeight.OnValueChangedObject += OnValueChanged;
            Message.OnValueChangedObject += OnValueChanged;

            RecalculateGeometry();
        }

        public static implicit operator Text(string constant)
        {
            return new Text(constant);
        }

        private void OnValueChanged(object oldObject, object newObject)
        {
            RecalculateGeometry();
        }

        private void RecalculateGeometry()
        {
            var typeface = new Typeface(Face.Value);
            BackingText = new FormattedText(
                Message.Value,
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                typeface,
                FontHeight.Value,
                new SolidColorBrush(Colors.Black));
            //formattedText.LineHeight = LineHeight;
            if (Modifiers.Value.HasFlag(FontModifier.Italic))
                BackingText.SetFontStyle(FontStyles.Italic);
            if (Modifiers.Value.HasFlag(FontModifier.Bold))
                BackingText.SetFontWeight(FontWeights.Bold);

            GlyphTypeface glyphTypeface;
            if (!typeface.TryGetGlyphTypeface(out glyphTypeface))
                throw new InvalidOperationException("Unsupported font!");
            GlyphTypeface = glyphTypeface;

            var bounds = new RectangleGeometry(new Rect(0, 0, BackingText.Width, BackingText.Height));
            bounds.Freeze();
            Bounds = bounds;

            if (OnChanged != null)
                OnChanged();
        }

        public event Action OnChanged;
    }
}
