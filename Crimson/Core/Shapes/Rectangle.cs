﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Crimson.Core.Shapes
{
    public class Rectangle : Shape
    {
        public Rect Rect { get; set; }

        public Rectangle(Rect rect)
        {
            Rect = rect;
            var bounds = new RectangleGeometry(Rect);
            bounds.Freeze();
            Bounds = bounds;
        }
    }
}
