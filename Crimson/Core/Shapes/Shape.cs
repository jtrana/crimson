﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Styles;
using System.Windows.Media;

namespace Crimson.Core.Shapes
{
    public class Shape
    {
        public Shape()
        {
            Id = this.GetHashCode();
            Fill = new SolidFillStyle(Colors.Black);
            Stroke = new StrokeStyle();
        }

        internal int Id { get; set; }

        public FillStyle Fill { get; set; }
        public StrokeStyle Stroke { get; set; }

        //what might be good here would be a Bounds possibly
        //separate from the actual underlying geometry
        //this would make hit testing more efficient in cases
        //like text where the bounding box is what is desired
        public virtual Geometry Bounds { get; protected set; }
    }
}
