﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Crimson.Core.Shapes
{
    public class Path : Shape
    {
        public Path(Geometry geo)
        {
            Geometry = geo;
            var bounds = new RectangleGeometry(geo.Bounds);
            bounds.Freeze();
            Bounds = bounds;
        }

        public virtual Geometry Geometry { get; set; }
    }
}
