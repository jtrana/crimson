﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Shapes;
using System.Windows;
using Crimson.Core.Bindings;
using Crimson.Core.Events.Input;

namespace Crimson.Core.Elements
{
    public class Button : Placeholder
    {
        public readonly UIBindable<Command> Command;

        public Button(Context ctx)
            : base(ctx)
        {
            IsPickable = true;

            Command = new UIBindable<Command>(this, null);
        }

        protected override void InternalRender()
        {
            var boundingBox = Children.First().ReportedSize.Value;
            Context.AddInvisible(new Rectangle(new Rect(boundingBox)));
        }

        public override bool OnPointUp(PointerUpEvent evt, Shape target, Point pickHit)
        {
            if (Command.Value != null)
                Command.Value();
            return true;
        }
    }
}
