﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Bindings;
using Crimson.Core.Shapes;
using System.Windows;

namespace Crimson.Core.Elements
{
    public class TextLine : Element
    {
        public TextLine(Context ctx)
            : base(ctx)
        {
            Text = new UIBindable<Text>(this, new Text());
            Text.Value.OnChanged += new Action(Value_OnChanged);
            Text.OnValueChanged += new OnValueChangedHandler<Shapes.Text>(Text_OnValueChanged);

            Message = new UIBindable<string>(this, string.Empty);
            Message.OnValueChanged += (o, n) => Text.Value.Message.Value = Message.Value;
        }

        void Text_OnValueChanged(Text oldValue, Text newValue)
        {
            if (oldValue != null)
                oldValue.OnChanged -= Value_OnChanged;
            if (newValue != null)
                newValue.OnChanged += Value_OnChanged;
        }

        void Value_OnChanged()
        {
            Message.Value = Text.Value.Message.Value;
            Text.MarkRenderDirty();
        }

        public readonly UIBindable<Text> Text;
        public readonly UIBindable<string> Message;

        protected override void InternalRender()
        {
            Context.Add(Text.Value);
            ReportedSize.Value = new Size(Text.Value.BackingText.Width, Text.Value.BackingText.Height);
        }
    }
}
