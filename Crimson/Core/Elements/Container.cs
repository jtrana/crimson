﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Bindings;
using System.Windows;
using Crimson.Core.Templates;
using System.Diagnostics;

namespace Crimson.Core.Elements
{
    public class Container<T> : Element
    {
        private bool m_LayoutCurrent;

        public readonly UIBindable<IList<T>> Source;

        public Container(Context ctx)
            : base(ctx)
        {
            Source = new UIBindable<IList<T>>(this, new List<T>());
            m_LayoutCurrent = false;
            Source.OnValueChangedObject += Source_OnValueChanged;
        }

        void Source_OnValueChanged(object oldValue, object newValue)
        {
            Debug.WriteLine("Clearing children");
            ClearChildren();
            Debug.WriteLine("Generating children");
            foreach (var childBacker in Source.Value)
            {
                var newChild = Context.ViewMapping.GenerateNonGeneric(childBacker);
                Debug.WriteLine("-Child " + newChild.GetHashCode());
                AddChild(newChild);
            }

            ForceUpdateLayout();
        }

        protected override void OnChildReportedSizeChanged(Size oldSize, Size newSize)
        {
            ForceUpdateLayout();
        }

        private void ForceUpdateLayout()
        {
            m_LayoutCurrent = false;
            Source.MarkRenderDirty();
        }

        protected override void InternalRender()
        {
            if (!m_LayoutCurrent)
            {
                PerformLayout();
                m_LayoutCurrent = true;
            }
        }

        protected virtual void PerformLayout()
        {
        }
    }
}
