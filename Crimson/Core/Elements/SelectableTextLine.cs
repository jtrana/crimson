﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Bindings;
using Crimson.Core.Shapes;
using System.Windows;
using System.Diagnostics;
using Crimson.Core.Events.Input;
using System.Windows.Media;
using Crimson.Core.Styles;
using Crimson.Core.Utils;
using System.Windows.Forms;

namespace Crimson.Core.Elements
{
    public class SelectableTextLine : Element
    {
        public SelectableTextLine(Context ctx)
            : base(ctx)
        {
            Text = new UIBindable<Text>(this, new Text());
            Text.Value.OnChanged += new Action(Value_OnChanged);
            Text.OnValueChanged += new OnValueChangedHandler<Shapes.Text>(Text_OnValueChanged);

            Message = new UIBindable<string>(this, string.Empty);
            Message.OnValueChanged += (o, n) => Text.Value.Message.Value = Message.Value;

            SelectionStart = new UIBindable<int>(this, -1);
            SelectionEnd = new UIBindable<int>(this, -1);
            SelectedText = new UIBindable<string>(this, string.Empty);

            Context.Keyboard.OnCharacter += OnCharacter;

            IsPickable = true;
            IsPointerLeaveTrackable = true;
        }

        private bool m_IsSelecting;
        private Point m_PointDown;
        private bool m_ControlDown;

        public readonly UIBindable<Text> Text;
        public readonly UIBindable<string> Message;
        public readonly UIBindable<int> SelectionStart;
        public readonly UIBindable<int> SelectionEnd;
        public readonly UIBindable<string> SelectedText;

        protected override void InternalRender()
        {
            var selectionArea = GetSelectionArea();
            if (selectionArea.HasValue)
                Context.Add(new Rectangle(selectionArea.Value) { Fill = new SolidFillStyle(Colors.Orange) });
            Context.Add(Text.Value);
        }

        void OnCharacter(VirtualKeyboard sender, char letter)
        {
            if (letter == 'c' && sender.IsControlDown())
                Context.Clipboard.Contents = SelectedText.Value;
        }

        void Text_OnValueChanged(Text oldValue, Text newValue)
        {
            if (oldValue != null)
                oldValue.OnChanged -= Value_OnChanged;
            if (newValue != null)
                newValue.OnChanged += Value_OnChanged;
            ReportedSize.Value = new Size(Text.Value.BackingText.Width, Text.Value.BackingText.Height);
        }

        void Value_OnChanged()
        {
            Message.Value = Text.Value.Message.Value;
            ReportedSize.Value = new Size(Text.Value.BackingText.Width, Text.Value.BackingText.Height);
            Text.MarkRenderDirty();
        }

        private Rect? GetSelectionArea()
        {
            if (SelectionStart.Value == -1 || SelectionEnd.Value == -1)
                return null;

            var startIndex = SelectionStart.Value;
            var endIndex = SelectionEnd.Value;
            if (endIndex < startIndex)
            {
                var tmp = endIndex;
                endIndex = startIndex;
                startIndex = tmp;
            }

            double startY = 0;
            double endY = Text.Value.BackingText.Height;
            double startX = 0;
            double endX = 0;


            var message = Text.Value.Message.Value;

            double totalWidth = 0.0;
            var glyphTypeface = Text.Value.GlyphTypeface;
            var fontSize = Text.Value.FontHeight.Value;

            for (int i = 0; i <= endIndex; i++)
            {
                char c = message[i];
                ushort glyph = glyphTypeface.CharacterToGlyphMap[c];
                if (i == startIndex)
                    startX = totalWidth*fontSize;
                totalWidth += glyphTypeface.AdvanceWidths[glyph];
                if (i == endIndex)
                    endX = totalWidth*fontSize;
            }

            return new Rect(startX, startY, endX-startX, endY-startY);
        }

        private void UpdateSelectedText()
        {
            if (SelectionStart.Value == -1 || SelectionEnd.Value == -1)
            {
                SelectedText.Value = string.Empty;
                return;
            }

            var startIndex = SelectionStart.Value;
            var endIndex = SelectionEnd.Value;
            if (endIndex < startIndex)
            {
                var tmp = endIndex;
                endIndex = startIndex;
                startIndex = tmp;
            }

            SelectedText.Value = Text.Value.Message.Value.Substring(startIndex, endIndex - startIndex + 1);
        }

        private int IndexOfPoint(Point pickHit)
        {
            var message = Text.Value.Message.Value;

            double totalWidth = 0.0;
            var glyphTypeface = Text.Value.GlyphTypeface;
            var fontSize = Text.Value.FontHeight.Value;

            for (int i = 0; i < message.Length; i++)
            {
                char c = message[i];
                ushort glyph = glyphTypeface.CharacterToGlyphMap[c];
                totalWidth += glyphTypeface.AdvanceWidths[glyph];
                if (totalWidth*fontSize >= pickHit.X)
                    return i;
            }
            return -1;
        }

        public override bool OnPointDown(PointerDownEvent evt, Shape target, Point pickHit)
        {
            var proposedStart = IndexOfPoint(pickHit);
            if (proposedStart >= 0)
                m_IsSelecting = true;
            m_PointDown = evt.Location;
            SelectionStart.Value = proposedStart;
            SelectionEnd.Value = proposedStart;
            UpdateSelectedText();
            Debug.WriteLine("Start index: " + proposedStart);
            return true;
        }

        public override bool OnPointMove(PointerMoveEvent evt, Shape target, Point pickHit)
        {
            if (!m_IsSelecting)
                return true;
            SelectionEnd.Value = IndexOfPoint(pickHit);
            UpdateSelectedText();
            return true;
        }

        public override bool  OnPointUp(PointerUpEvent evt, Shape target, Point pickHit)
        {
            if (m_PointDown == evt.Location)
            {
                SelectionStart.Value = -1;
                SelectionEnd.Value = -1;
                UpdateSelectedText();
            }
             m_IsSelecting = false;
 	         return true;
        }

        public override void OnPointerLeave()
        {
            m_IsSelecting = false;
        }
    }
}
