﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Bindings;
using System.Windows.Media;
using System.Windows;

namespace Crimson.Core.Elements
{
    public enum StackerOrientation
    {
        Vertical,
        Horizontal
    }

    public enum StackerAlignment
    {
        TopOrLeft,
        Center,
        BottomOrRight
    }

    public class Stacker<T> : Container<T>
    {
        public readonly UIBindable<StackerOrientation> Orientation;

        public Stacker(Context ctx)
            : base(ctx)
        {
            Orientation = new UIBindable<StackerOrientation>(this, StackerOrientation.Vertical);
        }


        protected override void PerformLayout()
        {
            if (Orientation.Value == StackerOrientation.Vertical)
            {
                double heightCntr = 0.0;
                double maxWidth = 0.0;
                foreach (var child in Children)
                {
                    child.Transform.Value = new TranslateTransform(0, heightCntr);
                    heightCntr += child.ReportedSize.Value.Height;
                    if (child.ReportedSize.Value.Width > maxWidth)
                        maxWidth = child.ReportedSize.Value.Width;
                    //don't do alignment for now TODO
                }
                ReportedSize.Value = new Size(maxWidth, heightCntr);
            }
            //don't do horizontal for now TODO
        }
    }
}
