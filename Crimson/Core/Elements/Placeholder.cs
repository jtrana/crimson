﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core;
using Crimson.Core.Bindings;
using System.Windows;

namespace Crimson.Core.Elements
{
    public class Placeholder : Element
    {
        public UIBindable<object> Source;

        public Placeholder(Context ctx)
            : base(ctx)
        {
            Source = new UIBindable<object>(this, null);
            Source.OnValueChanged += new OnValueChangedHandler<object>(Source_OnValueChanged);
        }

        void Source_OnValueChanged(object oldValue, object newValue)
        {
            //regenerate children
            ClearChildren();
            var newChild = Context.ViewMapping.GenerateNonGeneric(newValue);
            AddChild(newChild);
        }

        protected override void OnChildReportedSizeChanged(Size oldSize, Size newSize)
        {
            ReportedSize.Value = newSize;
        }

        protected override void OnParentReportedSizeChanged(Size oldSize, Size newSize)
        {

        }
    }
}
