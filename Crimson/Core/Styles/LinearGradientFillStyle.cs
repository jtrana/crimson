﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Crimson.Core.Styles
{
    public class LinearGradientFillStyle : FillStyle
    {
        public Gradient Gradient { get; set; }
        public Point Start { get; set; }
        public Point End { get; set; }

        public LinearGradientFillStyle()
        {
            Gradient = new Gradient();
        }
    }
}
