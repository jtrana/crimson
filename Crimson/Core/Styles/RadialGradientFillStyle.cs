﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Crimson.Core.Styles
{
    public class RadialGradientFillStyle : FillStyle
    {
        public Gradient Gradient { get; set; }
        public Point CenterA { get; set; }
        public double RadiusA { get; set; }
        public Point CenterB { get; set; }
        public double RadiusB { get; set; }

        public RadialGradientFillStyle()
        {
            Gradient = new Gradient();
        }
    }
}
