﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Crimson.Core.Styles
{
    public class SolidStrokeStyle : StrokeStyle
    {
        public SolidStrokeStyle(Color color, double width)
        {
            Color = color;
            Width = width;
        }

        public Color Color { get; set; }
    }
}
