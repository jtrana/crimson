﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Crimson.Core.Styles
{
    public class Gradient
    {
        public List<Tuple<double, Color>> Stops { get; private set; }

        public Gradient()
        {
            Stops = new List<Tuple<double, Color>>();
        }

        public void Add(double offset, Color color)
        {
            Stops.Add(new Tuple<double, Color>(offset, color));
            Stops.Sort((a, b) => a.Item1.CompareTo(b.Item1));
        }
    }
}
