﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Crimson.Core.Styles
{
    public class SolidFillStyle : FillStyle
    {
        public SolidFillStyle(Color color)
        {
            Color = color;
        }

        public Color Color { get; set; }
    }
}
