﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crimson.Core.Utils
{
    public static class FluentCollectionExtensions
    {
        public static IDictionary<T, U> FAdd<T, U>(this IDictionary<T,U> dict, T key, U value)
        {
            dict.Add(key, value);
            return dict;
        }

        public static void AddPlus<T>(this ICollection<T> coll, params T[] items)
        {
            foreach (var item in items)
                coll.Add(item);
        }
    }
}
