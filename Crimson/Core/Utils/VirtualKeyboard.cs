﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Crimson.Core.Utils
{
    public class VirtualKeyboard
    {
        private static readonly Dictionary<Keys, char> m_LowerCaseMap;
        private static readonly Dictionary<Keys, char> m_UpperCaseMap;

        static VirtualKeyboard()
        {
            m_LowerCaseMap = new Dictionary<Keys, char>()
                {
                    { Keys.D1, '1' },
                    { Keys.D2, '2' },
                    { Keys.D3, '3' },
                    { Keys.D4, '4' },
                    { Keys.D5, '5' },
                    { Keys.D6, '6' },
                    { Keys.D7, '7' },
                    { Keys.D8, '8' },
                    { Keys.D9, '9' },
                    { Keys.D0, '0' },
                    { Keys.Subtract, '-' },
                    { Keys.Add, '=' },
                    { Keys.OemOpenBrackets, '[' },
                    { Keys.OemCloseBrackets, ']' },
                    { Keys.OemPipe, '\\' },
                    { Keys.OemSemicolon, ';' },
                    { Keys.OemQuotes, '\'' },
                    { Keys.Oemcomma, ',' },
                    { Keys.OemPeriod, '.' },
                    { Keys.OemQuestion, '/' },

                    { Keys.A, 'a' },
                    { Keys.B, 'b' },
                    { Keys.C, 'c' },
                    { Keys.D, 'd' },
                    { Keys.E, 'e' },
                    { Keys.F, 'f' },
                    { Keys.G, 'g' },
                    { Keys.H, 'h' },
                    { Keys.I, 'i' },
                    { Keys.J, 'j' },
                    { Keys.K, 'k' },
                    { Keys.L, 'l' },
                    { Keys.M, 'm' },
                    { Keys.N, 'n' },
                    { Keys.O, 'o' },
                    { Keys.P, 'p' },
                    { Keys.Q, 'q' },
                    { Keys.R, 'r' },
                    { Keys.S, 's' },
                    { Keys.T, 't' },
                    { Keys.U, 'u' },
                    { Keys.V, 'v' },
                    { Keys.W, 'w' },
                    { Keys.X, 'x' },
                    { Keys.Y, 'y' },
                    { Keys.Z, 'z' },

                    { Keys.Tab, '\t' },
                    { Keys.Enter, '\n' },
                    { Keys.Space, ' ' }
                };

            m_UpperCaseMap = new Dictionary<Keys, char>()
                {
                    { Keys.D1, '!' },
                    { Keys.D2, '@' },
                    { Keys.D3, '#' },
                    { Keys.D4, '$' },
                    { Keys.D5, '%' },
                    { Keys.D6, '^' },
                    { Keys.D7, '&' },
                    { Keys.D8, '*' },
                    { Keys.D9, '(' },
                    { Keys.D0, ')' },
                    { Keys.Subtract, '_' },
                    { Keys.Add, '+' },
                    { Keys.OemOpenBrackets, '{' },
                    { Keys.OemCloseBrackets, '}' },
                    { Keys.OemPipe, '|' },
                    { Keys.OemSemicolon, ':' },
                    { Keys.OemQuotes, '\"' },
                    { Keys.Oemcomma, '<' },
                    { Keys.OemPeriod, '>' },
                    { Keys.OemQuestion, '?' },

                    { Keys.A, 'A' },
                    { Keys.B, 'B' },
                    { Keys.C, 'C' },
                    { Keys.D, 'D' },
                    { Keys.E, 'E' },
                    { Keys.F, 'F' },
                    { Keys.G, 'G' },
                    { Keys.H, 'H' },
                    { Keys.I, 'I' },
                    { Keys.J, 'J' },
                    { Keys.K, 'K' },
                    { Keys.L, 'L' },
                    { Keys.M, 'M' },
                    { Keys.N, 'N' },
                    { Keys.O, 'O' },
                    { Keys.P, 'P' },
                    { Keys.Q, 'Q' },
                    { Keys.R, 'R' },
                    { Keys.S, 'S' },
                    { Keys.T, 'T' },
                    { Keys.U, 'U' },
                    { Keys.V, 'V' },
                    { Keys.W, 'W' },
                    { Keys.X, 'X' },
                    { Keys.Y, 'Y' },
                    { Keys.Z, 'Z' },

                    { Keys.Tab, '\t' },
                    { Keys.Enter, '\n' },
                    { Keys.Space, ' ' }
                };
        }

        private Dictionary<Keys, bool> m_Modifiers;
        private HashSet<Keys> m_SpecialKeys;
        private HashSet<Keys> m_DualKeys;

        private bool m_IsCapsLockEngaged;

        public VirtualKeyboard()
        {
            m_IsCapsLockEngaged = false;

            m_Modifiers = new Dictionary<Keys, bool>();
            AddModifiers(
                Keys.ShiftKey,
                Keys.ControlKey,
                Keys.Alt);

            m_SpecialKeys = new HashSet<Keys>();
            m_SpecialKeys.AddPlus(
                Keys.CapsLock,
                Keys.Back,
                Keys.Delete,
                Keys.Insert,
                Keys.Home,
                Keys.End,
                Keys.PageUp,
                Keys.PageDown,
                Keys.Left,
                Keys.Right,
                Keys.Up,
                Keys.Down,
                Keys.Escape
                );

            m_DualKeys = new HashSet<Keys>();
            m_DualKeys.AddPlus(
                Keys.Tab,
                Keys.Enter,
                Keys.Space
                );
        }

        private void AddModifiers(params Keys[] keys)
        {
            foreach (var key in keys)
                m_Modifiers.Add(key, false);
        }

        public void SendKey(Keys key, bool isKeyDown)
        {
            if (m_Modifiers.ContainsKey(key))
            {
                m_Modifiers[key] = isKeyDown;
                return;
            }

            bool specialKeyOnly = m_SpecialKeys.Contains(key);
            if(specialKeyOnly || m_DualKeys.Contains(key))
            {
                if (isKeyDown)
                {
                    if(key==Keys.CapsLock)
                        m_IsCapsLockEngaged = !m_IsCapsLockEngaged;
                    if (OnSpecialKeyDown != null)
                        OnSpecialKeyDown(this, key);
                }
                else
                {
                    if (OnSpecialKeyUp != null)
                        OnSpecialKeyUp(this, key);
                }
            }

            if (specialKeyOnly)
                return;

            //rest is character handling, which will only occur on key up
            if (isKeyDown)
                return;

            //note that caps lock is not handled in the normal way
            //it is treated as though shift were being held down
            //why? I like it that way... I've always thought the
            //caps lock functionality is wrong. Besides, it makes
            //it easier to do this: )*)(**^*%&^$^%#
            var isShiftDown = m_Modifiers[Keys.ShiftKey] | m_IsCapsLockEngaged;
            var source = isShiftDown ? m_UpperCaseMap : m_LowerCaseMap;
            char result;
            if (source.TryGetValue(key, out result))
                if (OnCharacter != null)
                    OnCharacter(this, result);
        }

        public bool IsShiftDown()
        {
            return m_Modifiers[Keys.ShiftKey];
        }

        public bool IsControlDown()
        {
            return m_Modifiers[Keys.ControlKey];
        }

        public bool IsAltDown()
        {
            return m_Modifiers[Keys.Alt];
        }

        public delegate void SpecialKeyHandler(VirtualKeyboard sender, Keys key);
        public delegate void CharacterHandler(VirtualKeyboard sender, char letter);


        public event SpecialKeyHandler OnSpecialKeyDown;
        public event SpecialKeyHandler OnSpecialKeyUp;

        public event CharacterHandler OnCharacter;
    }
}
