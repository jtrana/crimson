﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crimson.Core.Utils
{
    public delegate void ClipboardContentsChangeHandler(object newValue);

    public class Clipboard
    {
        private object m_Contents;
        private object m_Lock = new object();

        public object Contents
        {
            get
            {
                lock (m_Lock)
                    return m_Contents;
            }
            set
            {
                lock (m_Lock)
                    m_Contents = value;
                if (OnContentsChanged != null)
                    OnContentsChanged(value);
            }
        }

        public event ClipboardContentsChangeHandler OnContentsChanged;
    }
}
