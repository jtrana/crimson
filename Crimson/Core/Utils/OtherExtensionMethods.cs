﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crimson.Core.Utils
{
    public static class OtherExtensionMethods
    {
        public static T As<T>(this object o) where T : class
        {
            return o as T;
        }
    }
}
