﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Shapes;
using System.Windows.Media;

namespace Crimson.Core.Events.Graphics
{
    public class RectangleAddedEvent : ShapeAddedEvent
    {
        public Rectangle Rectangle { get; private set; }


        public RectangleAddedEvent(Rectangle rectangle, Matrix transform)
            : base(rectangle, transform)
        {
            Rectangle = rectangle;
        }
    }
}
