﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Crimson.Core.Shapes;

namespace Crimson.Core.Events.Graphics
{
    public class PathAddedEvent : ShapeAddedEvent
    {
        public Path Path { get; set; }

        public PathAddedEvent(Path path, Matrix transform)
            : base(path, transform)
        {
            Path = path;
        }
    }
}
