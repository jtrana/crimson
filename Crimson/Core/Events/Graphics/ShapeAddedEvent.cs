﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Shapes;
using System.Windows.Media;

namespace Crimson.Core.Events.Graphics
{
    public class ShapeAddedEvent
    {
        public Shape Shape { get; set; }
        public Matrix Transform { get; set; }

        public ShapeAddedEvent(Shape shape, Matrix transform)
        {
            Shape = shape;
            Transform = transform;
        }
    }
}
