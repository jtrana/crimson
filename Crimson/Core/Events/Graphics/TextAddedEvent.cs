﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Shapes;
using System.Windows.Media;

namespace Crimson.Core.Events.Graphics
{
    public class TextAddedEvent : ShapeAddedEvent
    {
        public Text Text { get; private set; }

        public TextAddedEvent(Text text, Matrix transform) : base(text,transform)
        {
            Text = text;
        }
    }
}
