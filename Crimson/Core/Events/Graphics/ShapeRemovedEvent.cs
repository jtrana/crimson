﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Shapes;

namespace Crimson.Core.Events.Graphics
{
    public class ShapeRemovedEvent
    {
        public int ShapeId { get; private set; }

        public ShapeRemovedEvent(Shape shape)
        {
            ShapeId = shape.GetHashCode();
        }
    }
}
