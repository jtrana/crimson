﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Crimson.Core.Events.Input
{
    public class PointerUpEvent
    {
        public Point Location { get; set; }

        public int ContactId { get; set; }

        public PointerUpEvent(double x, double y, int contactId)
        {
            Location = new Point(x, y);
            ContactId = contactId;
        }
    }
}
