﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Crimson.Core.Events.Input
{
    public class PointerDownEvent
    {
        public Point Location { get; private set; }
        public int ContactId { get; private set; }

        public PointerDownEvent(double x, double y, int contactId)
        {
            Location = new Point(x, y);
            ContactId = contactId;
        }
    }
}
