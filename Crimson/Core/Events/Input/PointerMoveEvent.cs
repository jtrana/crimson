﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Crimson.Core.Events.Input
{
    public class PointerMoveEvent
    {
        public Point Location { get; private set; }

        public PointerMoveEvent(double x, double y)
        {
            Location = new Point(x, y);
        }
    }
}
