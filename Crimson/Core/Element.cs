﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Crimson.Core.Shapes;
using Crimson.Core.Events.Input;
using System.Windows;
using Crimson.Core.Bindings;

namespace Crimson.Core
{
    public delegate void Command();
    public delegate void ScalarCommand(double oldValue, double newValue);
    public delegate void VectorCommand(Vector oldVector, Vector newVector);


    public class Element : IDisposable
    {
        #region Initialization

        public readonly Context Context;
        
        public Element(Context context)
        {
            Parent = new UIBindable<Element>(this, null);
            Children = new List<Element>();

            Context = context;

            Transform = new UIBindable<Transform>(this, System.Windows.Media.Transform.Identity);
            ReportedSize = new UIBindable<Size>(this, new Size(0, 0));

            IsPickable = false;
        }

        #endregion

        #region Size/Location

        public UIBindable<Transform> Transform { get; private set; }
        public UIBindable<Size> ReportedSize { get; private set; }

        public Transform CumulativeTransform
        {
            get
            {
                var current = this;
                var group = new TransformGroup();
                while (current != null)
                {
                    group.Children.Insert(0, current.Transform.Value);
                    current = current.Parent.Value;
                }
                return group;
            }
        }

        #endregion

        #region Tree Management

        //Don't add/remove directly, use methods below
        public List<Element> Children { get; private set; }

        public readonly Bindable<Element> Parent;

        public void AddChild(Element child)
        {
            child.Parent.Value = this;
            ReportedSize.OnValueChanged += child.OnParentReportedSizeChanged;
            child.ReportedSize.OnValueChanged += OnChildReportedSizeChanged;
            Children.Add(child);
        }

        public void RemoveChild(Element child)
        {
            var index = Children.IndexOf(child);
            if (index < 0)
                throw new KeyNotFoundException("Could not find child to remove");
            Children.RemoveAt(index);
            UnhookChild(child);
        }

        private void UnhookChild(Element child)
        {
            child.ReportedSize.OnValueChanged -= OnChildReportedSizeChanged;
            ReportedSize.OnValueChanged -= child.OnParentReportedSizeChanged;
            child.Parent.Value = null;
        }

        public void ClearChildren()
        {
            foreach (var child in Children)
            {
                UnhookChild(child);
                child.Dispose();
            }
            Children.Clear();
        }

        #endregion

        #region Render

        public void Render()
        {
            using (var session = Context.OpenElementSession(this))
                InternalRender();
            foreach (var child in Children)
                child.Render();
        }

        protected virtual void InternalRender()
        {
        }

        public void Dispose()
        {
            Context.Cleanup(this);
        }

        #endregion

        #region Input Events

        public bool IsPickable { get; protected set; }
        public bool IsPointerLeaveTrackable { get; protected set; }

        protected virtual void OnParentReportedSizeChanged(Size oldSize, Size newSize) { }
        protected virtual void OnChildReportedSizeChanged(Size oldSize, Size newSize) { }

        public virtual bool OnPointUp(PointerUpEvent evt, Shape target, Point pickHit)
        {
            return true;
        }

        public virtual bool OnPointDown(PointerDownEvent evt, Shape target, Point pickHit)
        {
            return true;
        }

        public virtual bool OnPointMove(PointerMoveEvent evt, Shape target, Point pickHit)
        {
            return true;
        }

        public virtual void OnPointerLeave()
        {
        }

        #endregion
    }
}
