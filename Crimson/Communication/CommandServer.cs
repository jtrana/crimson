﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fleck;
using System.Diagnostics;
using Crimson.Core;

namespace Crimson.Communication
{
    public class CommandServer
    {
        private List<ApplicationInstance> m_ApplicationServers;
        private WebSocketServer m_Server;

        public void Initialize(Action<Context> createRoot)
        {
            m_ApplicationServers = new List<ApplicationInstance>();
            var singleton = new ApplicationInstance();
            singleton.Initialize(8182, createRoot);
            m_ApplicationServers.Add(singleton);

            m_Server = new WebSocketServer("ws://localhost:8181");
            m_Server.Start(socket =>
            {
                socket.OnOpen = () => SendRedirect(socket);
                socket.OnClose = () => { };
                socket.OnMessage = message => { };
            });
        }

        private void SendRedirect(IWebSocketConnection socket)
        {
            var msg = "{type:'cr', location:'ws://localhost:8182'}";
            Debug.WriteLine(msg);
            socket.Send(msg);
        }
    }
}
