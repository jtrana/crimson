﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Styles;
using Crimson.Core.Shapes;
using System.Windows.Media;
using System.Windows;
using Crimson.Core.Events.Graphics;

namespace Crimson.Communication
{
    public static class EventSerializer
    {
        public static string ToString(ShapeAddedEvent evt, string type, string tag)
        {
            return "{type:'" + type + "',sid:" + evt.Shape.Id + ", stroke:" + ToString(evt.Shape.Stroke) + ", fill:" + ToString(evt.Shape.Fill) + ", trans:" + ToString(evt.Transform) + ","+tag+"}";
        }

        public static string ToString(RectangleAddedEvent evt)
        {
            return ToString(evt, "mra", "rect:" + ToString(evt.Rectangle.Rect));
        }

        public static string ToString(TextAddedEvent evt)
        {
            return ToString(evt, "mta", "text:" + ToString(evt.Text));
        }

        public static string ToString(PathAddedEvent evt)
        {
            return ToString(evt, "mpa", "path:"+PathConverter.Convert(evt.Path.Geometry));
        }

        public static string ToString(ShapeRemovedEvent evt)
        {
            return "{type:'msr',sid:" + evt.ShapeId + "}";
        }

        public static string ToString(RenderTimeEvent evt)
        {
            return "{type:'mrt'}";
        }

        public static string ToString(SolidFillStyle evt)
        {
            return "{type:'sfs',color:" + EventSerializer.ToString(evt.Color) + "}";
        }

        public static string ToString(LinearGradientFillStyle evt)
        {
            return "{type:'lfs',start:" + ToString(evt.Start) + ",end:" + ToString(evt.End) + ",grad:" + ToString(evt.Gradient) + "}";
        }

        public static string ToString(RadialGradientFillStyle evt)
        {
            return "{type:'rfs',centerA:" + ToString(evt.CenterA) + ",radiusA:" + evt.RadiusA.ToString("0.#") + ",centerB:" + ToString(evt.CenterB) + ",radiusB:" + evt.RadiusB.ToString("0.#") + ",grad:" + ToString(evt.Gradient) + "}";
        }

        public static string ToString(FillStyle fillStyle)
        {
            if (fillStyle == null)
                return "''";
            var sfs = fillStyle as SolidFillStyle;
            if (sfs != null)
                return ToString(sfs);
            var lfs = fillStyle as LinearGradientFillStyle;
            if (lfs != null)
                return ToString(lfs);
            var rfs = fillStyle as RadialGradientFillStyle;
            if (rfs != null)
                return ToString(rfs);
            return "''";
        }

        public static string ToString(Point point)
        {
            return StringFromArray(point.X.ToString("0.#"), point.Y.ToString("0.#"));
        }

        public static string ToString(Gradient gradient)
        {
            var builder = new StringBuilder();
            builder.Append("[");
            foreach (var stop in gradient.Stops)
            {
                builder.Append(stop.Item1.ToString("0.##"));
                builder.Append(",");
                builder.Append(stop.Item2.R);
                builder.Append(",");
                builder.Append(stop.Item2.G);
                builder.Append(",");
                builder.Append(stop.Item2.B);
                builder.Append(",");
                builder.Append(stop.Item2.A);
                builder.Append(",");
            }
            if (gradient.Stops.Count > 1)
                builder.Remove(builder.Length - 1, 1);
            builder.Append("]");
            return builder.ToString();
        }

        public static string ToString(SolidStrokeStyle sss)
        {
            return "{type:'sss', color:" + ToString(sss.Color) + ", width:" + sss.Width + "}";
        }

        public static string ToString(StrokeStyle strokeStyle)
        {
            if (strokeStyle == null)
                return "''";
            var sss = strokeStyle as SolidStrokeStyle;
            if (sss != null)
                return ToString(sss);
            return "''";
        }

        public static string ToString(Matrix matrix)
        {
            return StringFromArray(matrix.M11, matrix.M12, matrix.M21, matrix.M22, matrix.OffsetX, matrix.OffsetY);
        }

        public static string ToString(Rect rect)
        {
            return StringFromArray(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        public static string ToString(Color color)
        {
            return StringFromArray(color.R, color.G, color.B, color.A);
        }

        public static string ToString(Text text)
        {
            var result = text.FontHeight.Value + "px/" + text.LineHeight.Value + "px " + text.Face.Value + ", sans-serif";
            if (text.Modifiers.Value.HasFlag(FontModifier.Italic))
                result = "italic " + result;
            if (text.Modifiers.Value.HasFlag(FontModifier.Bold))
                result = "bold " + result;
            return "{text:'" + EscapeStringForJavaScript(text.Message.Value) + "', font:'" + result + "'}";
        }

        public static string StringFromArray(params object[] values)
        {
            return "[" + string.Join(",", values) + "]";
        }

        /// <summary>
        /// Prepares plaintext to be used as the text in a JavaScript string.
        /// </summary>
        /// <param name="input">String to escape.</param>
        /// <returns>Escaped version of the input string.</returns>
        /// Need to replace this TODO
        public static string EscapeStringForJavaScript(string input)
        {
            input = input.Replace("'", @"\'");
            input = input.Replace("\b", @"\b");
            input = input.Replace("\t", @"\t");
            input = input.Replace("\n", @"\n");
            input = input.Replace("\f", @"\f");
            input = input.Replace("\r", @"\r");
            input = input.Replace("\"", "\\\"");
            return input;
        }
    }
}
