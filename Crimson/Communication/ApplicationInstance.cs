﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fleck;
using System.Diagnostics;
using Crimson.Core.Events.Input;
using Crimson.Core;
using System.Text.RegularExpressions;
using Crimson.Core.Events.Graphics;
using Crimson.Controls;
using System.Windows.Media;
using System.Threading;
using Crimson.Core.Templates;
using Crimson.Core.Bindings;

namespace Crimson.Communication
{
    public class ApplicationInstance
    {
        private Context m_Context;
        private IWebSocketConnection m_Socket;
        private Action<Context> m_AppInitializer;

        public void Initialize(int port, Action<Context> appInitializer)
        {
            m_AppInitializer = appInitializer;
            var server = new WebSocketServer("ws://localhost:"+port);
            server.Start(socket =>
            {
                socket.OnOpen = () => CreateClient(socket);
                socket.OnClose = () => DestroyClient();
                socket.OnMessage = message => ParseMessage(message);
            });
        }

        public void CreateClient(IWebSocketConnection socket)
        {
            m_Socket = socket;
            m_Context = new Context();
            m_Context.Initialize(this);

            m_AppInitializer(m_Context);
        }

        public void DestroyClient()
        {
        }


        private void ParseMessage(string message)
        {
            //Debug.WriteLine("---------->" + message);

            var type = ExtractString("type", message);
            switch (type)
            {
                case "ku":
                    var kuCode = ExtractInt("code", message);
                    m_Context.ReceiveEventKeyUp(kuCode);
                    break;
                case "kd":
                    var kdCode = ExtractInt("code", message);
                    m_Context.ReceiveEventKeyDown(kdCode);
                    break;
                case "pu":
                    var puX = ExtractInt("x", message);          
                    var puY = ExtractInt("y", message);
                    var pucid = ExtractInt("cid", message);
                    m_Context.ReceiveEvent(new PointerUpEvent(puX, puY, pucid));
                    break;
                case "pd":
                    var pdX = ExtractInt("x", message);
                    var pdY = ExtractInt("y", message);
                    var pdcid = ExtractInt("cid", message);
                    m_Context.ReceiveEvent(new PointerDownEvent(pdX, pdY, pdcid));
                    break;
                //think about the performance implications of this..!
                case "pm":
                    var pmX = ExtractInt("x", message);
                    var pmY = ExtractInt("y", message);
                    m_Context.ReceiveEvent(new PointerMoveEvent(pmX, pmY));
                    break;
            }
        }

        private int ExtractInt(string name, string message)
        {
            var codeMatch = Regex.Match(message, name + ":([-]?[0-9]+)");
            if (codeMatch.Success)
                return int.Parse(codeMatch.Groups[1].Value);
            throw new InvalidOperationException();
        }

        private string ExtractString(string name, string message)
        {
            var match = Regex.Match(message, name + ":['\"]([A-Za-z]+)['\"]");
            if (match.Success)
                return match.Groups[1].Value;
            throw new InvalidOperationException();
        }

        public void Send(ShapeRemovedEvent evt)
        {
            var msg = EventSerializer.ToString(evt);
            Debug.WriteLine("Thread "+Thread.CurrentThread.ManagedThreadId+" "+ msg);
            m_Socket.Send(msg);
        }

        public void Send(RectangleAddedEvent evt)
        {
            var msg = EventSerializer.ToString(evt);
            //Debug.WriteLine(msg);
            m_Socket.Send(msg);
        }

        public void Send(TextAddedEvent evt)
        {
            var msg = EventSerializer.ToString(evt);
            Debug.WriteLine("Thread " + Thread.CurrentThread.ManagedThreadId + " " + msg);
            m_Socket.Send(msg);
        }

        public void Send(PathAddedEvent evt)
        {
            var msg = EventSerializer.ToString(evt);
            Debug.WriteLine("Thread " + Thread.CurrentThread.ManagedThreadId + " " + msg);
            m_Socket.Send(msg);
        }

        public void Send(RenderTimeEvent evt)
        {
            var msg = EventSerializer.ToString(evt);
            ///Debug.WriteLine(msg);
            m_Socket.Send(msg);
        }
    }
}
