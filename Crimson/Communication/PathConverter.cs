﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Diagnostics;

namespace Crimson.Communication
{
    public static class PathConverter
    {
        public static string Convert(Geometry rawGeometry)
        {
            var flattened = rawGeometry.GetFlattenedPathGeometry();
            var builder = new StringBuilder();
            builder.Append("[");
            foreach (var figure in flattened.Figures)
            {
                builder.AppendFormat("['M',{0:0.#},{1:0.#}],", figure.StartPoint.X, figure.StartPoint.Y);
                foreach (var segment in figure.Segments)
                {
                    var arc = segment as ArcSegment;
                    if (arc != null)
                    {
                        throw new NotSupportedException("Have not yet seen this after flattening. Difficult to support directly because it doesn't have a correspondent in HTML5 canvas spec, .arc* are circular there. Would have to approximate as Bezier segments. TODO for future?");
                    }
                    var bezier = segment as BezierSegment;
                    if (bezier != null)
                    {
                        AppendCubicBezier(builder, bezier.Point1, bezier.Point2, bezier.Point3, bezier.IsSmoothJoin);
                        continue;
                    }
                    var qb = segment as QuadraticBezierSegment;
                    if (qb != null)
                    {
                        AppendQuadraticBezier(builder, qb.Point1, qb.Point2, qb.IsSmoothJoin);
                        continue;
                    }
                    var line = segment as LineSegment;
                    if (line != null)
                    {
                        AppendLine(builder, line.Point, line.IsSmoothJoin);
                        continue;
                    }
                    var pb = segment as PolyBezierSegment;
                    if (pb != null)
                    {
                        for (int i = 0; i < pb.Points.Count; i += 3)
                            AppendCubicBezier(builder, pb.Points[i], pb.Points[i + 1], pb.Points[i + 2], pb.IsSmoothJoin);
                        continue;
                    }
                    var pl = segment as PolyLineSegment;
                    if (pl != null)
                    {
                        foreach (var pl_Line in pl.Points)
                            AppendLine(builder, pl_Line, pl.IsSmoothJoin);
                        continue;
                    }
                    var pqb = segment as PolyQuadraticBezierSegment;
                    if (pqb != null)
                    {
                        for (int i = 0; i < pqb.Points.Count; i += 2)
                            AppendQuadraticBezier(builder, pb.Points[i], pb.Points[i + 1], pqb.IsSmoothJoin);
                        continue;
                    }
                    throw new NotSupportedException("Unsupported segment type: " + segment.GetType());
                }
                builder.Append("['Z'],");
            }
            if (builder[builder.Length - 1] == ',')
                builder.Remove(builder.Length - 1, 1);
            builder.Append("]");
            return builder.ToString();
        }

        private static void AppendLine(StringBuilder builder, Point line, bool isSmooth)
        {
            builder.AppendFormat("['L',{0:0.#},{1:0.#},{2}],", line.X, line.Y, isSmooth ? 1 : 0);
        }

        private static void AppendCubicBezier(StringBuilder builder, Point point1, Point point2, Point point3, bool isSmooth)
        {
            builder.AppendFormat("['C',{0:0.#},{1:0.#},{2:0.#},{3:0.#},{4:0.#},{5:0.#},{6}],",
                            point1.X,
                            point1.Y,
                            point2.X,
                            point2.Y,
                            point3.X,
                            point3.Y,
                            isSmooth ? 1 : 0);
        }

        private static void AppendQuadraticBezier(StringBuilder builder, Point point1, Point point2, bool isSmooth)
        {
            builder.AppendFormat("['Q',{0:0.#},{1:0.#},{2:0.#},{3:0.#},{4}],",
                            point1.X,
                            point1.Y,
                            point2.X,
                            point2.Y,
                            isSmooth ? 1 : 0);
        }



        public static void Test()
        {
            var geo = new EllipseGeometry(new Rect(0, 5, 20, 30));
            var pathGeo = new PathGeometry();
            var converter = new GeometryConverter();
            string geoString = (string)converter.ConvertTo((Geometry)geo, typeof(string));
            Debug.WriteLine(geoString);
        }
    }

    
}
