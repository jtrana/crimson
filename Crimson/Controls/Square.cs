﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core;
using Crimson.Core.Shapes;
using System.Windows;
using System.Windows.Media;
using Visual = Crimson.Core.Element;
using Crimson.Core.Styles;
using Crimson.Core.Bindings;
using Crimson.Core.Templates;

namespace Crimson.Controls
{
    public class Square : Visual
    {
        static Square()
        {
            DefaultStyleRegistry.Register<Square>(r => r.Fill.Value = Colors.Blue);
        }

        public readonly UIBindable<Color> Fill;
        public readonly UIBindable<Color> Stroke;
        

        public Square(Context ctx)
            : base(ctx)
        {
            Fill = new UIBindable<Color>(this, Colors.Blue);
            Stroke = new UIBindable<Color>(this, Colors.Orange);
        }

        protected override void InternalRender()
        {
            var rectangle = new Rectangle(new Rect(0, 0, 20, 20));
            rectangle.Fill = new SolidFillStyle(Fill.Value);
            rectangle.Stroke = new SolidStrokeStyle(Stroke.Value, 1);
            Context.Add(rectangle);
        }
    }
}
