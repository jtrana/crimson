﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Shapes;
using System.Windows;
using Crimson.Core.Events.Input;
using Crimson.Core;
using System.Windows.Media;
using Visual = Crimson.Core.Element;
using System.Runtime.InteropServices;
using Crimson.Core.Styles;
using Crimson.Core.Bindings;
using System.Windows.Forms;

namespace Crimson.Controls
{
    public class TestElement : Element
    {
        public TestElement(Context ctx)
            : base(ctx)
        {
            Text = new UIBindable<string>(this, string.Empty);
            FillColor = new UIBindable<Color>(this, Colors.Red);

            ctx.Keyboard.OnCharacter += new Core.Utils.VirtualKeyboard.CharacterHandler(Keyboard_OnCharacter);
            ctx.Keyboard.OnSpecialKeyUp += new Core.Utils.VirtualKeyboard.SpecialKeyHandler(Keyboard_OnSpecialKeyUp);

            IsPickable = true;
        }

        void Keyboard_OnSpecialKeyUp(Core.Utils.VirtualKeyboard sender, System.Windows.Forms.Keys key)
        {
            if (key == Keys.Back && Text.Value.Length > 0)
                Text.Value = Text.Value.Substring(0, Text.Value.Length - 1);
        }

        void Keyboard_OnCharacter(Core.Utils.VirtualKeyboard sender, char letter)
        {
            Text.Value += letter;
        }

        public readonly UIBindable<string> Text;
        public readonly UIBindable<Color> FillColor;

        

        protected override void InternalRender()
        {
            //var rectangle = new Rectangle(new Rect(0, 0, 100, 100));
            //rectangle.Fill = new SolidFillStyle(Colors.Green);
            //Context.Add(rectangle);
            var streamGeometry = new StreamGeometry();
            using (var ctx = streamGeometry.Open())
            {
                ctx.BeginFigure(new Point(50, 50), true, true);
                ctx.ArcTo(new Point(100, 50), new Size(30, 30), 200, false, SweepDirection.Clockwise, true, true);
                ctx.LineTo(new Point(100, 100), true, true);
            }
            var path = new Path(streamGeometry);
            //var path = new Rectangle(new Rect(0, 0, 200, 200));
            //var linearGradient = new LinearGradientFillStyle();
            //linearGradient.Start = new Point(0, 0);
            //linearGradient.End = new Point(100.0, 0);
            //linearGradient.Gradient.Add(0.0, Colors.Aqua);
            //linearGradient.Gradient.Add(0.9, Colors.Pink);
            //linearGradient.Gradient.Add(1.0, Colors.Purple);
            //path.Fill = linearGradient;
            var radialGradient = new RadialGradientFillStyle();
            radialGradient.CenterA = new Point(0, 0);
            radialGradient.RadiusA = 30;
            radialGradient.CenterB = new Point(100, 100);
            radialGradient.RadiusB = 20;
            radialGradient.Gradient.Add(0, Colors.Red);
            radialGradient.Gradient.Add(0.2, Colors.Black);
            radialGradient.Gradient.Add(0.8, Colors.Plum);
            path.Fill = radialGradient;
            path.Stroke = new SolidStrokeStyle(Colors.Black, 3);
            Context.Add(path);
            //Context.Push(new TranslateTransform(30, 30));
            var text = new Text();
            text.Fill = new SolidFillStyle(Colors.Coral);
            text.Message.Value = Text.Value;
            //Context.Add(text);
            //Context.Pop();
        }

        public override bool OnPointMove(PointerMoveEvent Event, Shape target, Point pickHit)
        {
            Color newColor = Colors.PapayaWhip;
            if (target.GetType() == typeof(Rectangle))
                newColor = Color.FromScRgb((float)1.0, (float)(pickHit.X / 100.0), (float)0, (float)0);
            else if (target.GetType() == typeof(Text))
                newColor = Color.FromScRgb((float)1.0, (float)0, (float)(pickHit.X / 100.0), (float)0);
            newColor.Clamp();
            FillColor.Value = newColor;
            return true;
        }
    }
}
