﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crimson.Core.Bindings;
using System.Windows.Media;
using System.Threading;
using Crimson.Core;
using System.Diagnostics;
using Crimson.Core.Shapes;

namespace Crimson.Controls
{
    public class DummyModule
    {

        public Element RootElement { get; private set; }
        private TestElement m_TestElement;
        private DummyVM m_DummyVM;
        private Context m_Context;

        public void Initialize(Context context)
        {
            m_Context = context;

            RegisterViewModels();
            SetupRootElement();

            StartADemo();
        }

        private void RegisterViewModels()
        {
            m_Context.ViewMapping.RegisterView<DummyVM>(
                vm =>
                    -Stock.TestElement(Fill: Constant(Colors.Blue))
                    //-Stock.Stacker<string>(Source: ()=>vm.SomeText.ToUI)
                    //-Stock.SelectableTextLine(Message:()=>vm.Message.ToUI, SelectedText: ()=>vm.SelectedText.ToVM)
                    //[
                    //    -Stock.TextLine(Message: ()=>vm.SelectedText.ToUI)
                    //            .With(Transform: Constant((Transform)new TranslateTransform(100,100)))
                    //]
                    //-Stock.Button(Command: ()=>vm.Command.ToUI)
                    //[
                        //-Stock.TestElement(
                        //            Fill: () => vm.Inner.Value.Color.TwoWay,
                        //            Text: Constant("Hello, Web!"))
                        //            .With(
                        //                Transform: Constant((Transform)new TranslateTransform(100, 100)))
                        //- Stock.Square(
                        //            Color: Constant(Colors.OrangeRed))
                        //            .With(
                        //                Transform: Constant((Transform)new TranslateTransform(30, 30)))
                        //- Stock.Placeholder(
                        //            Source: () => vm.SourceObject.ToUI)
                        
                    //]

                    );

            m_Context.ViewMapping.RegisterView<PlaceholderAVM>(
                vm => -Stock.TextLine(Text: Constant((Text)"Hello world"))
                    );

            m_Context.ViewMapping.RegisterView<PlaceholderBVM>(
                vm => -Stock.TextLine(Text: Constant((Text)"Hello world"))
                    );

            m_Context.ViewMapping.RegisterView<string>(
                vm => -Stock.SelectableTextLine(Text: Constant((Text)vm))
                    );
            
        }

        private static Binder<T> Constant<T>(T value)
        {
            return () => new BindableBindingWrapper<T>(new Bindable<T>(value), BindingMode.ToUI);
        }

        private void SetupRootElement()
        {
            //there will be something to handle this eventually
            //just pass a vm instance and it will bind to a window blah blah blah

            var dummyVM = new DummyVM();
            Debug.WriteLine("Start");
            RootElement = m_Context.ViewMapping.Generate(dummyVM);
            Debug.WriteLine("Stop");

            using (m_Context.OpenRenderSession())
                RootElement.Render();

            m_DummyVM = dummyVM;
        }

        private void StartADemo()
        {
            Thread animator = new Thread(DoAnimation);
            animator.IsBackground = true;
            animator.Start();
        }

        private class PlaceholderAVM
        {
            public PlaceholderAVM()
            {
            }
        }

        private class PlaceholderBVM
        {
            public PlaceholderBVM()
            {
            }
        }

        private class InnerVM
        {
            public readonly Bindable<Color> Color = Colors.Black;
        }

        private class DummyVM
        {
            public readonly Bindable<InnerVM> Inner = new InnerVM();
            public readonly Bindable<object> SourceObject = new PlaceholderAVM();
            public readonly Bindable<Command> Command;
            public readonly Bindable<string> Message = new Bindable<string>("Hello World");
            public readonly Bindable<string> SelectedText = new Bindable<string>(string.Empty);

            public readonly Bindable<IList<string>> SomeText = new Bindable<IList<string>>(new List<string>{"A", "AB", "ABC"});

            public DummyVM()
            {
                Command = new Bindable<Command>(() => this.Message.Value = "Goodbye!");
            }
        }


        private void DoAnimation()
        {
            Thread.Sleep(5000);
            m_DummyVM.SourceObject.Value = new PlaceholderBVM();
            int i = 0;
            while (true)
            {
                var newColor = Color.FromRgb((byte)(i % 255), 0, 0);
                var newInnerVM = new InnerVM();
                newInnerVM.Color.Value = newColor;
                using (m_Context.OpenRenderSession())
                    m_DummyVM.Inner.Value = newInnerVM;
                Thread.Sleep(10000);
                i+=5;
                m_DummyVM.SomeText.Value = Enumerable.Range(0, i).Reverse().Take(5).Select(v=>v.ToString()).ToList();
            }
        }
    }
}
